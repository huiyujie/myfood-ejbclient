package edu.osu.cse5234.business.view;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="CUSTOMER_ORDER_LINE_ITEM")
public class LineItem implements Serializable{
	private int id;
	private int itemId;
	private String itemName;
	private int quantity;
	private String description;
	private double unitPrice;
	
	public LineItem() {
		super();
	}
	
	public LineItem(Item item) {
		id = item.getId();
		itemId = item.getItemNumber();
		itemName = item.getName();
		quantity = 0;
		description = item.getDescription();
		unitPrice = item.getUnitPrice();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="ITEM_ID")
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	
	@Column(name="ITEM_NAME")
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	@Column(name="QUANTITY")
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	@Transient
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@Transient
	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
}
