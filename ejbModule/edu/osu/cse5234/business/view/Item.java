package edu.osu.cse5234.business.view;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="ITEM")
public class Item implements Serializable{
	private int id;
	private int itemNumber;
	private String name;
	private String description;
	private int quantity;
	private double unitPrice;
 
 public Item() {
 }
 
 public Item(String name, int quantity) {
  this.name = name;
  this.quantity = quantity;
 }
 
 public Item(String name, int quantity, double unitPrice) {
  super();
  this.name = name;
  this.quantity = quantity;
  this.unitPrice = unitPrice;
 }

 @Id
 @GeneratedValue(strategy = GenerationType.IDENTITY)
 @Column(name="ID")
 public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}
@Column(name="ITEM_NUMBER")
public int getItemNumber() {
	return itemNumber;
}

public void setItemNumber(int itemNumber) {
	this.itemNumber = itemNumber;
}

@Column(name="UNIT_PRICE")
public double getUnitPrice() {
  return unitPrice;
 }

 public void setUnitPrice(double unitPrice) {
  this.unitPrice = unitPrice;
 }

 @Column(name="NAME")
 public String getName() {
  return name;
 }
 public void setName(String name) {
  this.name = name;
 }
 @Column(name="AVAILABLE_QUANTITY")
 public int getQuantity() {
  return quantity;
 }
 public void setQuantity(int quantity) {
  this.quantity = quantity;
 }

 @Column(name="DESCRIPTION")
public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}
 
}
