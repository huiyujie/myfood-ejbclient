package edu.osu.cse5234.business.view;

import java.io.Serializable;
import java.util.List;

public class Inventory implements Serializable{
	public List<Item> inventory;

	
	public Inventory() {
		super();
	}

	public Inventory(List<Item> inventory) {
		super();
		this.inventory = inventory;
	}

	public List<Item> getInventory() {
		return this.inventory;
	}

	public void setInventory(List<Item> inventory) {
		this.inventory = inventory;
	}


	
}
